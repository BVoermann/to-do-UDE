from django import forms
from .models import TodoItem


class TodoForm(forms.ModelForm):
    class Meta:
        model = TodoItem
        fields = ["title", "description"]
        widgets = {
            "title": forms.TextInput(attrs={"class": "title"}),
            "description": forms.Textarea(attrs={"class": "description"}),
        }


class DeleteItemForm(forms.Form):
    item_id = forms.IntegerField(widget=forms.HiddenInput())
