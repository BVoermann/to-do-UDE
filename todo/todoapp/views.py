from django.shortcuts import render
from django.views.generic import TemplateView

from .models import TodoItem
from .forms import TodoForm, DeleteItemForm


class TodoView(TemplateView):
    template_name = "todo_list.html"

    def get(self, request):
        todos = TodoItem.objects.all()
        form = TodoForm()

        return render(request, template_name=self.template_name, context={"todos": todos, "form": form})

    def post(self, request):
        if "delete" in request.GET.get("method", []):
            delete_form = DeleteItemForm(request.POST)
            if delete_form.is_valid():
                print("form valid")
                item_pk = delete_form.cleaned_data["item_id"]
                TodoItem.objects.filter(pk=item_pk).delete()
            else:
                print("form invalid")
                print(delete_form.errors)
        elif "add" in request.GET.get("method", []):
            create_form = TodoForm(request.POST)
            if create_form.is_valid():
                create_form.save()
        else:
            pass
        form = TodoForm()
        todos = TodoItem.objects.all()
        return render(request, template_name=self.template_name, context={"todos": todos, "form": form})
