from django.urls import path
from todoapp.views import TodoView

app_name = "todoapp"
urlpatterns = [
    path("", TodoView.as_view(), name="index-de"),
]
